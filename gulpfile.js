var gulp = require('gulp'),
    pjfolder = 'html_mytaxi_drivers_tracking',
    clean = require('gulp-clean'),
    htmlmin = require('gulp-htmlmin'),
    cleanCSS = require('gulp-clean-css'),
    imagemin = require('gulp-imagemin'),
    browserSync = require("browser-sync");


gulp.task('clean', function () { // Создаем таск очистки dist -
    return gulp.src('dist/', {read: false})
        .pipe(clean())
        .pipe(browserSync.reload({stream: true})); // Обновляем CSS на странице при изменении
});


gulp.task('htmlmin', function () {
    return gulp.src('app/*.html')
        .pipe(htmlmin({collapseWhitespace: true}))
        .pipe(gulp.dest('dist'));
});


gulp.task('clean-css', function () {
    return gulp.src('app/styles/style.css')
        .pipe(cleanCSS({compatibility: '*'}))
        .pipe(gulp.dest('dist/styles'));
});


gulp.task('imagemin', function () {
    gulp.src('app/images/**/*')
        .pipe(imagemin())
        .pipe(gulp.dest('dist/images'))
});


gulp.task('fonts', function () {
    return gulp.src('app/fonts/*.otf') // Создаем таск переноса файлов видео
        .pipe(gulp.dest('dist/fonts')) // Берем источник
        .pipe(browserSync.reload({stream: true})) // Обновляем файлы на странице при изменении
});

gulp.task('css', function () {
    return gulp.src(['app/styles/normalize.css', 'app/styles/fonts.css']) // Создаем таск переноса файлов видео
        .pipe(gulp.dest('dist/styles/')) // Берем источник
        .pipe(browserSync.reload({stream: true})) // Обновляем файлы на странице при изменении
});

gulp.task('js', function () {
    return gulp.src('app/js/*.js') // Создаем таск переноса файлов видео
        .pipe(gulp.dest('dist/js/')) // Берем источник
        .pipe(browserSync.reload({stream: true})) // Обновляем файлы на странице при изменении
});

gulp.task('build', [
    'clean',
    'htmlmin',
    'clean-css',
    'imagemin',
    'fonts',
    'css',
    'js',
]);